package uk.ac.uea.parkingaid;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final String PREFS_NAME = "TicketInfo";
    public static Activity main;
    private Button addVehicle;
    private Button btnNote;
    private TextView txtTime;
    private TextView txtNote;
    private TicketModel ticket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toolbar
        Toolbar tb = (Toolbar)  findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        main = this;

        addVehicle = (Button)findViewById(R.id.btnAddVehicle);
        txtTime = (TextView)findViewById(R.id.txtTime);
        txtNote = (TextView)findViewById(R.id.txtNote);
        btnNote = (Button)findViewById(R.id.btnNote);

        //Scrollable Note
        txtNote.setMovementMethod(new ScrollingMovementMethod());

        Calendar c = Calendar.getInstance();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm MMM dd");

        //Load in ticket information if available
        SharedPreferences ticketInfo = getSharedPreferences(PREFS_NAME, 0);
        if(ticketInfo.contains("finishAt")){
            Date currentDate = c.getTime();
            long finishAtTime = ticketInfo.getLong("finishAt", 0);
            long currentTime = currentDate.getTime();

            Log.d(TAG, "Loading in saved ticket data...");
            double latitude = Double.longBitsToDouble(ticketInfo.getLong("latitude", 0));
            double longitude = Double.longBitsToDouble(ticketInfo.getLong("longitude", 0));
            Date createdAt = new Date(ticketInfo.getLong("createdAt", 0));
            Date finishAt = new Date(ticketInfo.getLong("finishAt", 0));
            int isSet = ticketInfo.getInt("isSet", 0);
            ticket = new TicketModel(latitude, longitude, createdAt, finishAt, isSet);

            if(ticketInfo.contains("note")){
                String note = ticketInfo.getString("note", "None");
                txtNote.setText(note);
                btnNote.setText(getString(R.string.note_btn_edit));
            }

            Log.d(TAG, "Lat: " + ticket.getLatitude() +
                    "\nLong: " + ticket.getLongitude() +
                    "\nisSet: " + ticket.getIsSet() +
                    "\nCreated At: " + ticket.getCreatedAt() +
                    "\nFinish At: " + ticket.getFinishAt());

            if(finishAtTime <= currentTime){
                txtTime.setText("Expired");
                Log.d(TAG, "Ticket Expired");
            }
            else {
                txtTime.setText(timeFormat.format(ticket.getFinishAt()));
            }
        }
        else {
            Log.d(TAG, "First time use");
            ticket = new TicketModel();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Toolbar Menu
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_setting){
            Intent intent = new Intent(this, AppSettings.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts the TrackActivity activity
     * @param view
     */
    public void startNewTrack(View view){
        Intent intent = new Intent(this, TrackActivity.class);
        startActivity(intent);
    }

    /**
     * Starts the CompassActivity
     * @param view
     */
    public void findCar(View view){
        if(ticket.getIsSet() != 0){
            Intent intent = new Intent(this, CompassActivity.class);
            intent.putExtra("ticket", ticket);
            intent.putExtra("latitude", ticket.getLatitude());
            intent.putExtra("longitude", ticket.getLongitude());
            intent.putExtra("finishTime", ticket.getFinishAt().getTime());
            startActivity(intent);
            this.finish();
        }
        else {
            notTrackedError(findViewById(R.id.activity_main));
        }
    }

    /**
     * Clears the current ticket data and cancels the pending notification.
     */
    public void clearTicket(){
        //Get preferences file
        SharedPreferences ticketInfo = getSharedPreferences(PREFS_NAME, 0);

        //Clear preferences file
        SharedPreferences.Editor editor = ticketInfo.edit();
        editor.clear();
        editor.commit();

        //Cancel Alarm
        Intent nIntent = new Intent(getApplicationContext(), NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, nIntent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        pendingIntent.cancel();
        alarmManager.cancel(pendingIntent);
        Log.d(TAG, "Alarm Cancelled");

        //Reset Ticket and TextView
        ticket = new TicketModel();
        txtTime.setText(getString(R.string.not_set));
        txtNote.setText(getString(R.string.default_note));
        btnNote.setText(getString(R.string.note_btn));
    }

    /**
     * Pop-up box to allow users to confirm clearing a ticket
     * @param view
     */
    public void checkClearTicket(View view){
        final Snackbar cleared = Snackbar.make(view, "Ticket cleared!", Snackbar.LENGTH_SHORT);
        DialogInterface.OnClickListener dcl = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
                switch(which){
                    case DialogInterface.BUTTON_POSITIVE:
                        clearTicket();
                        cleared.show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Clear ticket?").setPositiveButton("Yes", dcl)
                .setNegativeButton("No", dcl).show();
    }

    /**
     * Starts the AppSettings activity
     * @param view
     */
    public void openSettings(View view){
        Intent intent = new Intent(this, AppSettings.class);
        startActivity(intent);
    }

    /**
     * Starts the NoteActivity activity
     * @param view
     */
    public void startNote(View view){
        if(ticket.getIsSet() != 0) {
            Intent intent = new Intent(this, NoteActivity.class);
            startActivity(intent);
        }
        else {
            notTrackedError(findViewById(R.id.activity_main));
        }
    }

    public void notTrackedError(View view){
        Log.e(TAG, "NOT SET");
        Snackbar error = Snackbar.make(view, "No vehicle being tracked!", Snackbar.LENGTH_SHORT);
        View errorView = error.getView();
        errorView.setBackgroundColor(Color.RED);
        error.setActionTextColor(Color.WHITE);
        error.show();
    }

}
