package uk.ac.uea.framework.implementation;

import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

/**
 * Created by ChanelleRichardson on 02/02/2017.
 */

/**
 * Class contains basic methods for creating dates and converting to and from string/Date format.
 * Makes sure date format follows UK standard: DD-MM-YYYY
 */

public class DateCreateAndConvert {

    public Date stringToDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy"); // makes sure date format follows UK standard.
        ParsePosition pos = new ParsePosition(0);
        Date convertedDate = dateFormat.parse(date, pos);
        return convertedDate;
    }

    public String dateToString(Date date) {
        String sqlDatePattern = "EEE MMM dd kk:mm:ss z yyyy";
        String newDatePattern = "dd-MM-yyyy"; // makes sure date format follows UK standard.
        SimpleDateFormat dateFormat = new SimpleDateFormat(newDatePattern);
        String openDayString = dateFormat.format(date);
        return openDayString;
    }

    public Date createDate(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, (month - 1)); //to convert month to correct format.
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

}
