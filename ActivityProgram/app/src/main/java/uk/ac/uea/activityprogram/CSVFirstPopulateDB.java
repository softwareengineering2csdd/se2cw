package uk.ac.uea.activityprogram;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import uk.ac.uea.framework.implementation.AndroidFileIO;

/**
 * Created by ChanelleRichardson on 05/02/2017.
 */

public class CSVFirstPopulateDB extends AsyncTask {
    private Context context;
    private AndroidFileIO afio;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private AssetManager assets;

    private static CSVFirstPopulateDB instance = null;

    private CSVFirstPopulateDB(Context context) {
        this.context = context;
    }


    public static CSVFirstPopulateDB getInstance(Context context) {
        if (instance == null) {
            instance = new CSVFirstPopulateDB(context);
        }
        return instance;
    }

    /**
     *
     * @param params
     * @return
     */
    @Override
    protected Object doInBackground(Object[] params) {
        try {
            insertFromCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * reads in the CSV file saved in the assets folder
     * starts a database transaction for each line, splits lines by commas, inserts them into the database then ends the transaction.
     * Faulty entries are caught in the catch block and the transaction is ended without setting it as successful.
     * Using transactions, no faulty entries can be entered into the database.
     * @throws IOException
     */
    public void insertFromCSV() throws IOException {
        assets = context.getAssets();
        dbHelper = DatabaseHelper.getInstance(context);
        db = dbHelper.getReadableDatabase();

        InputStreamReader isr = new InputStreamReader(context.getAssets().open("starterData.csv"));
        BufferedReader buff = new BufferedReader(isr);

        String line;
        while ((line = buff.readLine()) != null) {
            try {
                db.beginTransaction();
                String[] entryData = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                int id = Integer.parseInt(entryData[0]);
                String title = entryData[1];
                String school = entryData[2];
                String roomNum = entryData[3];
                String time = entryData[4];
                String date = entryData[5];
                boolean saved = false;

                if (dbHelper.getExistingActivityByID(id)) {
                    dbHelper.updateActivities(id, title, school, roomNum, time, date, dbHelper.getExistingSaved(id));
                    //                   dbHelper.updateActivities(id, title, school, roomNum, time, date, dbHelper.getExistingSaved(id));

                } else {
                    dbHelper.insertActivities(id, title, school, roomNum, time, date, saved);
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            } catch (Exception e) {
                Log.e("CSV2DB entry failed:", e.toString());
                //Throw error to user if inserting an activity fails.
                db.endTransaction();

            }
        }
    }
}
