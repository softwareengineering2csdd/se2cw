package uk.ac.uea.activityprogram;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SchoolActivity extends AppCompatActivity {
    private static final String TAG = "SchoolActivity";
    private DatabaseHelper dbHelper;
    private String selectedSchool, selectedDate;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ActivityModel> activityList;
//    private ArrayList<SavedActivityModel> savedList;
    private CheckBox remind;
    View.OnClickListener snackbarListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);
        dbHelper = DatabaseHelper.getInstance(this);
        Intent i = getIntent();
        TextView schoolTitle = (TextView) findViewById(R.id.schoolNameTxt);
        selectedSchool = i.getStringExtra("selectedSchool");
        selectedDate = i.getStringExtra("selectedDate");
        schoolTitle.setText(selectedSchool);

        activityList = dbHelper.getActivitiesBySchoolAndDate(selectedSchool, selectedDate);

        recyclerView = (RecyclerView) findViewById(R.id.activitiesRecycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ActivityRecyclerAdapter(this, activityList);
        recyclerView.setAdapter(adapter);

        }

    @Override
    protected void onResume() {
        super.onResume();
        activityList = dbHelper.getActivitiesBySchoolAndDate(selectedSchool, selectedDate);
    }

}
