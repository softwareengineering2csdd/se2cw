package uk.ac.uea.parkingaid;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Danny on 01/12/2016.
 */

public class TicketModel implements Parcelable {

    private double latitude;
    private double longitude;
    private int isSet; // 0 = not set, 1 = set
    private Date createdAt;
    private Date finishAt;

    public TicketModel(){
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.isSet = 0;
        this.createdAt = new Date((long)0);
        this.finishAt = new Date((long)0);
    }

    public TicketModel(double latitude, double longitude, Date createdAt, Date finishAt, int isSet){
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdAt = createdAt;
        this.finishAt = finishAt;
        this.isSet = isSet;
    }

    /**
     * Allows the object to be passed via an intent
     * @param in
     */
    public TicketModel(Parcel in){
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.isSet = in.readInt();
        this.createdAt = new Date(in.readLong());
        this.finishAt = new Date(in.readLong());
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getIsSet() {
        return isSet;
    }

    public void setIsSet(int isSet) {
        this.isSet = isSet;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getFinishAt() {
        return finishAt;
    }

    public void setFinishAt(Date finishAt) {
        this.finishAt = finishAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeInt(this.isSet);
        dest.writeLong(this.createdAt.getTime());
        dest.writeLong(this.finishAt.getTime());
    }

    public static final Parcelable.Creator<TicketModel> CREATOR = new Parcelable.Creator<TicketModel>() {

        public TicketModel createFromParcel(Parcel in) {
            return new TicketModel(in);
        }

        public TicketModel[] newArray(int size) {
            return new TicketModel[size];
        }
    };

}
