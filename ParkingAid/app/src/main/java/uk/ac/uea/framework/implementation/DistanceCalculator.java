package uk.ac.uea.framework.implementation;

/**
 * Class to calculate distance between 2 sets of lat/long.
 */
public class DistanceCalculator {

    /**
     * Method to calculate distance between 2 sets of lat/long.
     * Unit = "M" for Statute Miles, "K" for Kilometers & "N" for Nautical Miles
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param unit
     * @return distance
     */
    public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit.contains("K")) {
            dist = dist * 1.609344;
        } else if (unit.contains("N")) {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /**
     * Method to convert decimal degrees to radians
     * @param deg
     * @return double
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * Method to convert radians to decimal degrees
     * @param rad
     * @return double
     */
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}
