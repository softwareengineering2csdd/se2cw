package uk.ac.uea.parkingaid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

//import uk.ac.uea.parkingaid.framework.DeviceLocation;
import uk.ac.uea.framework.implementation.DeviceLocation;

public class TrackActivity extends AppCompatActivity {
    public SharedPreferences prefs;
    private static final String TAG = "TrackActivity";
    private static final String PREFS_NAME = "TicketInfo";
    private EditText txtTicketTime;
    private DeviceLocation location;
    private SimpleDateFormat timeFormat;
    public static final long HOUR = 3600*1000; // in milli-seconds.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newtrack);

        location = new DeviceLocation(this);
        txtTicketTime = (EditText) findViewById(R.id.txtTicketTime);
        timeFormat = new SimpleDateFormat("HH:mm");
    }

    /**
     * Creates a new ticket with the information provided by the user and current location data
     * provided by the DeviceLocation class. Saves the ticket data to a SharedPreferences file and
     * initialises a notification to display at a set time before ticket expiration (specified by
     * the user in the settings activity)
     * @param view The current view
     */
    public void startTracking(View view) {
        TicketModel ticket;
        double longitude;
        double latitude;
        boolean goodDate; //Checks if valid date has been entered
        boolean gpsActive;
        String remainingTimeTxt = txtTicketTime.getText().toString(); // User Input
        Date ticketTime = null; // Time Entered
        Date createdAt; // Current DateTime
        Date finishAt; // Ticket Expiration DateTime

        //Messages
        Snackbar error = Snackbar.make(view, "Invalid time entered!", Snackbar.LENGTH_SHORT);
        Snackbar errorGPS = Snackbar.make(view, "A Working GPS signal is required!", Snackbar.LENGTH_SHORT);
        View errorView = error.getView();
        errorView.setBackgroundColor(Color.RED);
        error.setActionTextColor(Color.WHITE);
        View errorViewGPS = errorGPS.getView();
        errorViewGPS.setBackgroundColor(Color.RED);
        errorGPS.setActionTextColor(Color.WHITE);

        //Checks if user input is valid
        try {
            ticketTime = timeFormat.parse(remainingTimeTxt);
            goodDate = true;
        } catch (ParseException e) {
            goodDate = false;
            Log.e(TAG, e.toString());
        }

        //Get location data
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        if(latitude == 0 && longitude == 0){
            gpsActive = false;
        }else{
            gpsActive = true;
        }

        //Checks that the input box is not empty and the user has entered a valid time
        if (!remainingTimeTxt.isEmpty() && goodDate && gpsActive) {

            //Create blank ticket
            ticket = new TicketModel();

            //Get current time
            Calendar c = Calendar.getInstance();
            createdAt = c.getTime();

            //Get finish time and convert to date
            long finishAtLong = createdAt.getTime() + ticketTime.getTime() + HOUR;
            finishAt = new Date(finishAtLong);

            Log.d(TAG, "createdAt: " + createdAt);
            Log.d(TAG, "finishAt: " + finishAt);



            //Set ticket data
            Log.d(TAG, "Latitude = " + latitude + "\nLongitude = " + longitude + "\nTime Remaining: " + remainingTimeTxt);
            ticket.setLatitude(latitude);
            ticket.setLongitude(longitude);
            ticket.setCreatedAt(createdAt);
            ticket.setFinishAt(finishAt);
            ticket.setIsSet(1); //isSet

            //Save ticket to shared prefs
            saveTicket(ticket);

            //Set notification
            setNotification(finishAtLong);

            //Create intent, go back to home screen.
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
            MainActivity.main.finish(); // Stops user from being able to go back to original main activity (before ticket was created)

        } else {
            Log.e(TAG, "Invalid time entered");

            if(!gpsActive) {
                errorGPS.show();
            }else
                error.show();
        }
    }

    /**
     * Saves the ticket information to a SharedPreferences file
     */
    public void saveTicket(TicketModel ticket){
        SharedPreferences ticketInfo = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = ticketInfo.edit();
        editor.clear(); // Clears any old ticket
        if(ticket != null) {
            long ft = ticket.getFinishAt().getTime();
            editor.putLong("latitude", Double.doubleToRawLongBits(ticket.getLatitude()));
            editor.putLong("longitude", Double.doubleToRawLongBits(ticket.getLongitude()));
            editor.putLong("createdAt", ticket.getCreatedAt().getTime());
            editor.putLong("finishAt", ticket.getFinishAt().getTime());
            editor.putInt("isSet", ticket.getIsSet());

            Log.d(TAG, "Data saved to SharedPreferences:  " + ft);
        }
        editor.apply();
    }

    /**
     * Sets up a notification which will appear at a set time before the ticket expires
     * @param finishAt The time the ticket is set to expire
     */
    public void setNotification(long finishAt){
        //Get notification time for SharedPreferences
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int timeSelected = Integer.parseInt(prefs.getString("notificationTime", "2"));
        Log.d(TAG, "Time Selected: " + timeSelected);

        //Calculate alarm time based off notification time offset
        long notificationTime = finishAt - TimeUnit.MINUTES.toMillis(timeSelected);
        Date notificationDate = new Date(notificationTime);

        //Set up alarm manager
        Intent nIntent = new Intent(getApplicationContext(), NotificationReceiver.class);
        nIntent.putExtra("finishAt", finishAt);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, nIntent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, notificationTime, pendingIntent);
        Log.d(TAG, "Alarm time set to: " + timeFormat.format(notificationDate));
    }

}
