package uk.ac.uea.framework.implementation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import uk.ac.uea.activityprogram.DatabaseHelper;

/**
 * Created by ChanelleRichardson on 06/02/2017.
 */

public class PopulateDBFramework extends AsyncTask {
    Context context;
    AndroidFileIO afio;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;

    private static PopulateDBFramework instance = null;

    private PopulateDBFramework(Context context) {
        this.context = context;
    }

    public PopulateDBFramework(AndroidFileIO afio, Context context, SQLiteDatabase db) {
        this.context = context;
        this.afio = new AndroidFileIO(this.context);
        this.db = db;
    }

    public static PopulateDBFramework getInstance(Context context) {
        if (instance == null) {
            instance = new PopulateDBFramework(context);
        }
        return instance;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            insertFromCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertFromCSV() throws IOException {

        dbHelper = DatabaseHelper.getInstance(context);
        db = dbHelper.getReadableDatabase();
        //BufferedReader buff = new BufferedReader(new InputStreamReader(afio.readFile("dataCSV.csv")));

        FileReader file = new FileReader(context.getExternalFilesDir("dataCSV.csv").toString());
        BufferedReader buff = new BufferedReader(file);

        String line;
        while ((line = buff.readLine()) != null) {
            try {
                db.beginTransaction();
                String[] entryData = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                int id = Integer.parseInt(entryData[0]);
                String title = entryData[1];
                String school = entryData[2];
                String roomNum = entryData[3];
                String time = entryData[4];
                String date = entryData[5];
                boolean saved = false;

                if (dbHelper.getExistingActivityByID(id)) {
                    dbHelper.updateActivities(id, title, school, roomNum, time, date, dbHelper.getExistingSaved(id));
                    //                   dbHelper.updateActivities(id, title, school, roomNum, time, date, dbHelper.getExistingSaved(id));

                } else {
                    dbHelper.insertActivities(id, title, school, roomNum, time, date, saved);
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
            catch (Exception e) {
                Log.e("CSV2DB entry failed:", e.toString());
                //Throw error to user if inserting an activity fails.
                db.endTransaction();

            }
        }
    }
}
