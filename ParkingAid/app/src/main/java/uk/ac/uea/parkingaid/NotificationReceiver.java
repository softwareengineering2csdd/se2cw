package uk.ac.uea.parkingaid;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ybm14yju on 12/12/2016.
 */

public class NotificationReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Intent nIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, nIntent, 0);

        //Get intent data
        long finishTimeLong = intent.getLongExtra("finishAt", 0);
        Date finishTime = new Date(finishTimeLong);

        //Creates a notification
        Notification noti = new Notification.Builder(context)
                .setContentTitle("Ticket expires at: " + timeFormat.format(finishTime))
                .setContentText("Your ticket is about to expire!")
                .setSmallIcon(R.drawable.compass)
                .setContentIntent(pIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        noti.flags = Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
    }
}
