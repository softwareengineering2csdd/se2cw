//package uk.ac.uea.locationfinder;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import java.util.List;
//
///**
// * Created by ybm14yju on 16/12/2016.
// * Modified xay12jsu
// */
//
//public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.ViewHolder> {
//
//    public static class ViewHolder extends RecyclerView.ViewHolder{
//        public TextView tvName;
//        public TextView tvType;
//        public Button btnViewDesc;
//
//        public ViewHolder(View itemView){
//            super(itemView);
//            tvName = (TextView)itemView.findViewById(R.id.txtLocationName);
//            tvType = (TextView)itemView.findViewById(R.id.txtLocationType);
//            btnViewDesc = (Button)itemView.findViewById(R.id.btnViewDesc);
//        }
//
//    }
//
//    private List<Location> mLocations;
//    private Context mContext;
//
//    public LocationsAdapter(Context context, List<Location> locations){
//        this.mLocations = locations;
//        this.mContext = context;
//    }
//
//    private Context getContext(){
//        return this.mContext;
//    }
//
//    public Location getLocation(int position){
//        return this.mLocations.get(position);
//    }
//
//    @Override
//    public LocationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Context context = parent.getContext();
//        LayoutInflater inflater = LayoutInflater.from(context);
//
//        View locationView = inflater.inflate(R.layout.item_location, parent, false);
//
//        ViewHolder viewHolder = new ViewHolder(locationView);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(LocationsAdapter.ViewHolder holder, int position) {
//        final Location location = mLocations.get(position);
//
//        holder.tvName.setText(location.getName());
//        holder.tvType.setText(location.getType());
//        holder.btnViewDesc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(v.getContext(), LocationDescriptionActivity.class);
//                intent.putExtra("locationData", location);
//                mContext.startActivity(intent);
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return mLocations.size();
//    }
//}

package uk.ac.uea.locationfinder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ybm14yju on 16/12/2016.
 * Modified xay12jsu
 */

public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvName;
        public TextView tvType;
        public Button btnViewDesc;

        public ViewHolder(View itemView){
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.txtLocationName);
            tvType = (TextView)itemView.findViewById(R.id.txtLocationType);
            btnViewDesc = (Button)itemView.findViewById(R.id.btnViewDesc);
        }

    }

    private List<LocationModel> mLocations;
    private Context mContext;

    public LocationsAdapter(Context context, List<LocationModel> locations){
        this.mLocations = locations;
        this.mContext = context;
    }

    private Context getContext(){
        return this.mContext;
    }

    public LocationModel getLocation(int position){
        return this.mLocations.get(position);
    }

    @Override
    public LocationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View locationView = inflater.inflate(R.layout.item_location, parent, false);

        ViewHolder viewHolder = new ViewHolder(locationView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LocationsAdapter.ViewHolder holder, int position) {
        final LocationModel lm = mLocations.get(position);

        holder.tvName.setText(lm.getName());
        holder.tvType.setText(lm.getBuildingType());
        holder.btnViewDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LocationDescriptionActivity.class);
                intent.putExtra("locationID", lm.getId());


                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }
}
