package uk.ac.uea.locationfinder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


/**
 * Created by ChanelleRichardson on 17/01/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 9;
    private static String DATABASE_NAME = "LPDB";

    private static DatabaseHelper instance = null;
    private static Context context = null;

    private static final String TAG = "locationsPlannerDb:";

    protected DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static DatabaseHelper getInstance(Context context){
        if(instance == null){
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    //Tables
    private static final String T_LOCATIONS = "tLocations"; // Table for all of the Locations on the UEA map.
    private static final String T_FAVOURITE = "tFavourite"; // Table for any locations that user may want to save as a favourite.

    //T_LOCATIONS Columns
    private static final String LOCATIONS_ID = "id";
    private static final String LOCATIONS_NAME = "name";
    private static final String LOCATIONS_DESCRIPTION = "description";
    private static final String LOCATIONS_LAT = "lat";
    private static final String LOCATIONS_LONG = "long";
    private static final String LOCATIONS_TYPE = "type";


    //T_FAVOURITE Columns
    private static final String FAVOURITE_ID = "id";
    private static final String FAVOURITE_SAVED = "saved";


    //Create table strings
    private static final String CREATE_T_LOCATIONS = "CREATE TABLE " + T_LOCATIONS
            + "( "
            + LOCATIONS_ID + " INTEGER PRIMARY KEY NOT NULL, "
            + LOCATIONS_NAME + " TEXT, "
            + LOCATIONS_DESCRIPTION + " TEXT, "
            + LOCATIONS_LAT + " REAL, "
            + LOCATIONS_LONG + " REAL,  "
            + LOCATIONS_TYPE + " TEXT  "
            + ")";


    private static final String CREATE_T_FAVOURITE = "CREATE TABLE " + T_FAVOURITE
            + "( "
            + FAVOURITE_ID + " INTEGER PRIMARY KEY NOT NULL, "
            + FAVOURITE_SAVED + " INTEGER "
            + ")";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_T_LOCATIONS);
        db.execSQL(CREATE_T_FAVOURITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + T_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + T_FAVOURITE);
    }

    /**
     * INSERT OR REPLACE entry into the location table
     * db.replace: Will insert a new entry with the id specified or Update and entry that matches the id given to the method.
     * @param id
     * @param buildingType
     * @param name
     * @param description
     * @param latitude
     * @param longitude
     * @param favourite
     * @return
     */
    //
    public boolean insertOrReplaceLocationEntry(Integer id, String buildingType, String name, String description, double latitude, double longitude,  boolean favourite ) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor check = db.rawQuery("SELECT * FROM " + T_LOCATIONS + " WHERE ID=" + id + "", null);
        if (check==null){

        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOCATIONS_ID, id);
        contentValues.put(LOCATIONS_TYPE, buildingType);
        contentValues.put(LOCATIONS_NAME, name);
        contentValues.put(LOCATIONS_DESCRIPTION, description);
        contentValues.put(LOCATIONS_LAT, latitude);
        contentValues.put(LOCATIONS_LONG, longitude);
        db.replace(T_LOCATIONS, null, contentValues);
        insertOrReplaceFavouriteEntry(id, favourite);

        return true;
    }

    /**
     * Deletes an entry that has same id passed into method.
     * @param id
     * @return
     */
    //
    public Integer deleteLocationEntry(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(T_LOCATIONS, LOCATIONS_ID + " = ? ", new String[]{Integer.toString(id)});
    }

    /**
     * returns an arrayList of all entries in the database table of locatoins.
     * @return
     */
    //
    public ArrayList<LocationModel> getAllLocationEntries() {
        ArrayList<LocationModel> locationsList = new ArrayList<LocationModel>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + T_LOCATIONS, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            int id = res.getInt(res.getColumnIndex(LOCATIONS_ID));
            String buildingType = res.getString(res.getColumnIndex(LOCATIONS_TYPE));
            String name = res.getString(res.getColumnIndex(LOCATIONS_NAME));
            String description = res.getString(res.getColumnIndex(LOCATIONS_DESCRIPTION));
            Double latitude = res.getDouble(res.getColumnIndex(LOCATIONS_LAT));
            Double longitude = res.getDouble(res.getColumnIndex(LOCATIONS_LONG));
            boolean favourite = checkIsFavourite(id);

            LocationModel lm = new LocationModel(id, buildingType, name, description, latitude, longitude, favourite);

            locationsList.add(lm);
            res.moveToNext();
        }
        return locationsList;
    }

    /**
     * Returns an ArrayList of all UEA map location types.
     * Method is to be used to allow user to select and see all locations of a specified type.
     * @param type
     * @return
     */

    public ArrayList<LocationModel> getAllLocationEntriesByType(String type) {
        ArrayList<LocationModel> locationsList = new ArrayList<LocationModel>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + T_LOCATIONS + " WHERE " + LOCATIONS_TYPE + " = " +  type + "", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            int id = res.getInt(res.getColumnIndex(LOCATIONS_ID));
            String buildingType = res.getString(res.getColumnIndex(LOCATIONS_TYPE));
            String name = res.getString(res.getColumnIndex(LOCATIONS_NAME));
            String description = res.getString(res.getColumnIndex(LOCATIONS_DESCRIPTION));
            Double latitude = res.getDouble(res.getColumnIndex(LOCATIONS_LAT));
            Double longitude = res.getDouble(res.getColumnIndex(LOCATIONS_LONG));
            boolean favourite = checkIsFavourite(id);
            LocationModel lm = new LocationModel(id, buildingType, name, description, latitude, longitude, favourite);
            locationsList.add(lm);
            res.moveToNext();
        }
        return locationsList;
    }

    /**
     * returns a LocationModel of an entry with the same id passed into the method.
     * @param givenID
     * @return
     */
    //
    public LocationModel getLocationEntryByID(Integer givenID) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + T_LOCATIONS + " WHERE " + LOCATIONS_ID + " = " +  givenID + "", null);
        LocationModel lm = new LocationModel();
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            int id = res.getInt(res.getColumnIndex(LOCATIONS_ID));
            String buildingType = res.getString(res.getColumnIndex(LOCATIONS_TYPE));
            String name = res.getString(res.getColumnIndex(LOCATIONS_NAME));
            String description = res.getString(res.getColumnIndex(LOCATIONS_DESCRIPTION));
            Double latitude = res.getDouble(res.getColumnIndex(LOCATIONS_LAT));
            Double longitude = res.getDouble(res.getColumnIndex(LOCATIONS_LONG));
            boolean favourite = checkIsFavourite(id);
            lm = new LocationModel(id, buildingType, name, description, latitude, longitude, favourite);
            res.moveToNext();
        }
        return lm;
    }

    /**
     * returns an ArrayList of all entries that match the passed in string in their title.
     * The string does not have to be a complete word to find a match.
     * @param searchTerm
     * @return
     */
    //
    public ArrayList<LocationModel> searchDatabase(String searchTerm) {
        ArrayList<LocationModel> locationsList = new ArrayList<LocationModel>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + T_LOCATIONS + " WHERE " + LOCATIONS_NAME + " LIKE '%" +  searchTerm + "%'", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            int id = res.getInt(res.getColumnIndex(LOCATIONS_ID));
            String buildingType = res.getString(res.getColumnIndex(LOCATIONS_TYPE));
            String name = res.getString(res.getColumnIndex(LOCATIONS_NAME));
            String description = res.getString(res.getColumnIndex(LOCATIONS_DESCRIPTION));
            Double latitude = res.getDouble(res.getColumnIndex(LOCATIONS_LAT));
            Double longitude = res.getDouble(res.getColumnIndex(LOCATIONS_LONG));
            boolean favourite = checkIsFavourite(id);
            LocationModel lm = new LocationModel(id, buildingType, name, description, latitude, longitude, favourite);
            locationsList.add(lm);
            res.moveToNext();
        }
        return locationsList;
    }

    /**
     * returns the current number of rows within the database.
     * @return
     */
    //
    public Integer numOfRowsTLocation() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + T_LOCATIONS + "", null);
        int size = cursor.getCount();
        cursor.close();
        return size;
    }


    //Table Favourite methods


    /**
     * INSERT OR REPLACE entry into the users Favourite locations table
     * db.replace: Will insert a new entry with the id specified or Update and entry that matches the id given to the method.
     * @param id
     * @param saved
     * @return
     */
    public boolean insertOrReplaceFavouriteEntry(Integer id, boolean saved) {
        int isSaved = saved? 1:0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor check = db.rawQuery("SELECT * FROM " + T_FAVOURITE + " WHERE ID=" + id + "", null);
        if (check==null){

        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(FAVOURITE_ID, id);
        contentValues.put(FAVOURITE_SAVED, isSaved);
        db.replace(T_FAVOURITE, null, contentValues);
        return true;
    }

    /**
     * Deletes an entry from the Favourite table that has same id passed into method.
     * @param id
     * @return
     */
    //
    public Integer deleteFavouriteEntry(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(T_FAVOURITE, FAVOURITE_ID + " = ? ", new String[]{Integer.toString(id)});
    }

    /**
     * returns true or false if an entry that has same id passed into method has been saved.
     * @param givenID
     * @return
     */
    //
    public boolean checkIsFavourite(Integer givenID){
        boolean isFavourite = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + T_FAVOURITE + " WHERE " + FAVOURITE_ID + " = " +  givenID + "", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            isFavourite = res.getInt(res.getColumnIndex(FAVOURITE_SAVED)) !=0;
            res.moveToNext();
        }
        res.close();
        return isFavourite;
    }

    /**
     * returns the number of entries the favourite table has.
     * @return
     */

    public Integer numOfRowsTFavourite() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + T_FAVOURITE + "", null);
        int size = cursor.getCount();
        cursor.close();
        return size;
    }



}
