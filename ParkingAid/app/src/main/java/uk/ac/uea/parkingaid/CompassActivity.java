package uk.ac.uea.parkingaid;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import uk.ac.uea.parkingaid.framework.Compass;
//import uk.ac.uea.parkingaid.framework.DistanceCalculator;
//import uk.ac.uea.parkingaid.framework.DeviceLocation;

import uk.ac.uea.framework.implementation.Compass;
import uk.ac.uea.framework.implementation.DistanceCalculator;
import uk.ac.uea.framework.implementation.DeviceLocation;

public class CompassActivity extends Activity implements SensorEventListener {

    public static final long HOUR = 3600*1000; // in milli-seconds.
    private static final String TAG = "CompassActivity";
    private Float azimut;  // View to draw a compass
    private Compass compass;
    private DeviceLocation location;
    private double targetLat;
    private double targetLong;
    private long finishTime;
    private TicketModel ticket;
    private float[] mGravity;
    private float[] mGeomagnetic;
    private CustomDrawableView mCustomDrawableView;
    private SensorManager mSensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compass = new Compass(this);
        location = new DeviceLocation(this);
        mCustomDrawableView = new CustomDrawableView(this);
        setContentView(mCustomDrawableView);    // Register the sensor listeners
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        //Get intent data
        Intent intent = getIntent();
        Bundle bundle = getIntent().getExtras();
        ticket = bundle.getParcelable("ticket");
        this.targetLat = intent.getDoubleExtra("latitude", 0);
        this.targetLong = intent.getDoubleExtra("longitude", 0);
        this.finishTime = intent.getLongExtra("finishTime", 0);
    }

    public class CustomDrawableView extends View {
        Paint paint = new Paint();
        String distanceMetric = getMetric();
        String distanceAbbr = getMetricAbbr(distanceMetric);

        public CustomDrawableView(Context context) {
            super(context);
            paint.setColor(0xff00ff00);
            paint.setStyle(Style.STROKE);
            paint.setStrokeWidth(2);
            paint.setAntiAlias(true);
        }

        public String getMetric(){
            //Get distance metric setting
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String distanceMetric = prefs.getString("distanceMetric", "K");
            Log.d(TAG, "Distance Metric: " + distanceMetric);
            return distanceMetric;
        }

        public String getMetricAbbr(String metric){
            if(metric.contains("K")){
                return "km";
            }
            else if(metric.contains("M")){
                return "m";
            }
            else if(metric.contains("N")){
                return "n";
            }

            return "None";
        }

        protected void onDraw(Canvas canvas) {
            Calendar c = Calendar.getInstance();

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            double distance = DistanceCalculator.distance(location.getLatitude(), location.getLongitude(), targetLat, targetLong, distanceMetric);
            //double distance = DistanceCalculator.distance(52.668333, 1.042944, targetLat, targetLong, distanceMetric);
            //double distance = DistanceCalculator.distance(location.getLatitude(), location.getLongitude(), 52.668333, 1.042944, distanceMetric);
            String distanceString = String.format("%.2f", distance);
            long timeLeft = finishTime - c.getTime().getTime();
            long seconds = timeLeft / 1000 %60;
            long minutes = timeLeft / 1000 /60 %60;
            long hours = timeLeft / 1000 /60 /60;

            Date timeLeftDate = new Date(timeLeft);
            String timer = timeFormat.format(timeLeftDate);

            int width = getWidth();
            int height = getHeight();
            int centerx = width/2;
            int centery = height/2;

            //Draw distance
            paint.setColor(Color.BLACK);
            paint.setTextSize(72);
            paint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText("Distance: " + distanceString + distanceAbbr, width/2, height-300, paint);

            //Draw timer
            paint.setColor(Color.BLACK);
            paint.setTextSize(72);
            paint.setTextAlign(Paint.Align.CENTER);
            if(finishTime < c.getTime().getTime()){
                canvas.drawText("Time Remaining: 00:00:00", width/2, height-400, paint);
            }
            else {
                canvas.drawText("Time Remaining: " +Long.toString(hours) +":" + Long.toString(minutes)+ ":" + Long.toString(seconds), width / 2, height - 400, paint);
            }

            //Draw compass
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), R.drawable.compass, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;

            int centreX = getWidth()/2  - imageWidth/2;
            int centreY = getHeight()/2 - imageHeight/2;

            // Rotate the canvas with the azimut
            if (azimut != null)
                canvas.rotate(-azimut*360/(2*3.14159f), centerx, centery-300);

            paint.setColor(0xff0000ff);
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.compass);
            canvas.drawBitmap(b, centreX/2, centreY/2-200, paint);
            paint.setColor(0xff00ff00);
        }
    }


    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {  }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimut = orientation[0]; // orientation contains: azimut, pitch and roll
                //azimut -= compass.bearing(location.getLatitude(), location.getLongitude(), 52.627652, 1.236680);
                azimut -= compass.bearing(location.getLatitude(), location.getLongitude(), this.targetLat, this.targetLong);
            }
        }
        mCustomDrawableView.invalidate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("ticketReturn", ticket);
        startActivity(intent);
        this.finish();

    }
}