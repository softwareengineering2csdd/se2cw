package uk.ac.uea.locationfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        //Activity not needed. Search is dynamic and updates the Main Activities RecyclerView of locations.
    }
}
