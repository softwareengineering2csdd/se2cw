package uk.ac.uea.activityprogram;

import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Rixh_ on 27/11/2016.
 */

public class ActivityRecyclerAdapter extends RecyclerView.Adapter<ActivityRecyclerAdapter.ViewHolder> {
    private static final String TAG = "Checkbox Adapter: ";
    private final Context context;
    private final ArrayList<ActivityModel> activitiesList;



    public ActivityRecyclerAdapter(Context context, ArrayList<ActivityModel> activitiesList) {
        this.context = context;
        this.activitiesList = activitiesList;
        //this.savedList = savedList;
    }

    /**
     * sets the view to match that of the parent.
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ActivityRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_activity_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView roomNumber;
        public TextView time;
        public CheckBox remind;
        public boolean dbIsSaved = true;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.titleTxt);
            roomNumber = (TextView) v.findViewById(R.id.roomNumTxt);
            time = (TextView) v.findViewById(R.id.timeTxt);
            remind = (CheckBox) v.findViewById(R.id.remindChk);
        }
    }

    /**
     * gets all the open day activities information from the database to be displayed in the recycler view
     * when an items checkbox in the recycler view is seleced, show user a message to confirm that it has been saved
     * take items that have been checked or unchecked and insert or remove them from the database
     * notifications for alerting a user when an openday activity is going to happen in a set time can be added here.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        //

        holder.title.setText(activitiesList.get(position).getTitle());
        holder.roomNumber.setText(activitiesList.get(position).getRoomNumber());
        holder.time.setText(activitiesList.get(position).getTime());
        final CheckBox remind = holder.remind;

        remind.setOnCheckedChangeListener(null);
        remind.setChecked(activitiesList.get(position).isSaved());


        remind.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            remind.setSelected(isChecked);
                        }
                    });

        remind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper dbh = DatabaseHelper.getInstance(context);
                String checkTxt = "Reminder set for " + activitiesList.get(position).getTitle();
                String uncheckTxt = "Reminder removed for " + activitiesList.get(position).getTitle();
                Snackbar check = Snackbar.make(v, checkTxt, Snackbar.LENGTH_LONG);
                Snackbar uncheck = Snackbar.make(v, uncheckTxt, Snackbar.LENGTH_LONG);

                try {
//
                    int eventID = activitiesList.get(position).getId();
                    boolean eventSaved = remind.isChecked();
                        dbh.insertSavedEvent(activitiesList.get(position).getId(), remind.isChecked());
                        ActivityModel am = activitiesList.get(position);
                        am.setSaved(remind.isChecked());
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }


                if (activitiesList.get(position).isSaved()) {
                    check.show();
                } else {
                    uncheck.show();
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return activitiesList.size();
    }


}
