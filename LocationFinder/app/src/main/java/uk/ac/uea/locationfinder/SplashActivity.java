package uk.ac.uea.locationfinder;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {


    private static boolean splashLoaded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/**
 *         Makes sure that the Splash Activity is only displayed for a set time once otherwise the Main Activity will be immediately displayed.

 */
        if (!splashLoaded) {
            setContentView(R.layout.activity_splash);
            int secondsDelayed = 1;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }, secondsDelayed * 600);

            splashLoaded = true;
        } else {
            Intent nextActivity = new Intent(SplashActivity.this, MainActivity.class);
            nextActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(nextActivity);
            finish();
        }
    }
}