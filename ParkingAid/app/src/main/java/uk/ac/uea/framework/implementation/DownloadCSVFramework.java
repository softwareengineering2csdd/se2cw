package uk.ac.uea.framework.implementation;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.ac.uea.framework.implementation.AndroidFileIO;


/**
 * Created by ChanelleRichardson on 03/02/2017.
 */

public class DownloadCSVFramework extends AsyncTask<String, Integer, String> {
    private Context context;
    private File file;

    private static DownloadCSVFramework instance = null;

    private DownloadCSVFramework(Context context) {
        this.context = context;
    }

    public static DownloadCSVFramework getInstance(Context context) {
        if (instance == null) {
            instance = new DownloadCSVFramework(context);
        }
        return instance;
    }

    @Override
    protected String doInBackground(String... fileURL) {
        InputStream is = null;
        FileOutputStream fos = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(fileURL[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            //check if connection is correct to avoid downloading a error log instead of the intended CSV file.
            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage();
            }

            //download the CSV file using the input and output streams
            is = new BufferedInputStream(urlConnection.getInputStream());

            // creates writable file within external file dir : /storage/emulated/0/Android/uk.ac.uea.locationfinder/files/ - Chanelle Nexus 6P
            String dir = context.getExternalFilesDir(null).toString();
            String fileName = "dataCSV.csv";
            fos = new FileOutputStream(dir + "/" + fileName);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = is.read(data)) != -1) {
                if (isCancelled()) {
                    is.close();
                    return null;
                }
                total += count;
                fos.write(data, 0, count);

            }
        } catch (Exception e) {
            Log.e("DLCSVFW: ", e.toString());
        } finally {
            try {
                if (fos != null) {
                    fos.flush();
                    fos.getFD().sync();
                    fos.close();
                }
                if (is != null)
                    is.close();
            } catch (IOException ignored) {
            }

            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }


}
