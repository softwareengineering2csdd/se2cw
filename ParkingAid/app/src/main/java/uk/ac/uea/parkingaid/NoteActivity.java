package uk.ac.uea.parkingaid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class NoteActivity extends AppCompatActivity {

    private final static String TAG = "NoteActivity";
    private final static String PREFS_NAME = "TicketInfo";
    private EditText editNewNote;
    private TextView txtNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        editNewNote = (EditText)findViewById(R.id.editNewNote);
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0);
        if(prefs.contains("note")){
            editNewNote.setText(prefs.getString("note", ""));
        }
    }

    /**
     * Adds a note to the ticket info SharedPreferences
     * @param view
     */
    public void addNote(View view){
        //Messages
        Snackbar noteConfirm = Snackbar.make(view, "Note Added", Snackbar.LENGTH_SHORT);

        //Get value entered into text box
        String note = editNewNote.getText().toString();
        Log.d(TAG, "Note = " + note);

        SharedPreferences ticketInfo = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = ticketInfo.edit();
        editor.putString("note", note);
        editor.commit();

        noteConfirm.show();

        //Back to home
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
        MainActivity.main.finish(); // Stops user from being able to go back to original main activity (before note was created)
    }
}
