package uk.ac.uea.locationfinder;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import uk.ac.uea.framework.implementation.DownloadCSVFramework;

public class MainActivity extends AppCompatActivity {
    private ArrayList<LocationModel> locations;
    private RecyclerView rvLocations;
    private LocationsAdapter locationsAdapter;
    private EditText searchBar;
    private DatabaseHelper DB;

    /**
     * onCreate gets an ArrayList of all location entries from the database and populates a RecyclerView.
     * Recycler view is modified when a user searches for a location.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DB = DatabaseHelper.getInstance(this);
        searchBar = (EditText) findViewById(R.id.eTxtSearch);


        firstTimeRun();

        locations = DB.getAllLocationEntries();
        rvLocations = (RecyclerView) findViewById(R.id.lstLocations);
        locationsAdapter = new LocationsAdapter(this, locations);
        rvLocations.setAdapter(locationsAdapter);
        rvLocations.setLayoutManager(new LinearLayoutManager(this));

        /**
         * Search Function: Will query the database for the search term in real time.
         * Matching entries will be returned and the RecyclerView will be updated to display matches.
         */
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchTxt = (String) searchBar.getText().toString();
                locations.clear();
                locations.addAll(DB.searchDatabase(searchTxt));
                locationsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Populates the database with data stored in external
     */
    public void populateDB() {
        try {
//            CSVPopulateDB fileio = CSVPopulateDB.getInstance(this);
//            fileio.insertFromCSV();
            CSVPopulateDBThread fileIO = CSVPopulateDBThread.getInstance(this);
            fileIO.execute().get();

            String howManyRows = DB.numOfRowsTLocation().toString();
            Log.d("rows: ", howManyRows);
        } catch (Exception e) {
            Log.e("PopulatingDB", e.toString());
        }
    }



    /**
     * Will populate the Database with data shipped with the app.

     */
    public void firstPopulationOfDB() {
        try {
            CSVFirstPopulateDB fileIO = CSVFirstPopulateDB.getInstance(this);
            fileIO.execute().get();

            String howManyRows = DB.numOfRowsTLocation().toString();
            Log.d("rows: ", howManyRows);
        } catch (Exception e) {
            Log.e("PopulatingDB", e.toString());
        }
    }

    /**
     * Downloads the latest CSV file of location data from google spreadsheets within a different thread.
     */

    public void downloadCSV() {

        try {
            DownloadCSVFramework DLCSV = DownloadCSVFramework.getInstance(MainActivity.this);
            DLCSV.execute("https://docs.google.com/spreadsheets/d/1ac7RqJJg0uqGyXiVLYEEVY57Ca1WN8MfV4HqCOrtUCo/pub?output=csv").get();
        } catch (Exception e) {
            Log.e("DownloadCSV: ", e.toString());
        }

    }

    /**
     * Checks that the device is connected to the internet.
     * @return true or false if there is or isnt an internet connection.
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * method checks the apps version code and saves it to the shared preferences.
     * based on App version (such as first run etc...), will check for an internet connection and populate the DB with correct data.
     */

    public void firstTimeRun() {
        final String PREFS_NAME = "firstTimeRunPref";
        final String PREF_VERSION_CODE = "versionCode";
        final int NO_EXISTING_VERSION = -1;
        SharedPreferences prefSettings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        int currentVersion = BuildConfig.VERSION_CODE;
        int savedVersion = prefSettings.getInt(PREF_VERSION_CODE, NO_EXISTING_VERSION);

        /**
         * App has been run before, version is the same. Check for internet connection and download CSV file to populate DB.
         * If no internet connection, do not download new CSV or populate DB again as DB will already hold local existing data.
         * Notify user of lack of internet connection. Can't download latest CSV.
         */
        if (currentVersion == savedVersion) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            }else if(!isOnline()){
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();
            }

        }
        /**
         * App has not been run before, check for internet connection and download new CSV file to populate DB.
         * If no internet connection, populate DB through CSV in assets folder.
         * Notify user of lack of internet connection if they wish to update DB entries
         */
        else if (savedVersion == NO_EXISTING_VERSION) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            } else if (!isOnline()) {
                firstPopulationOfDB();
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();

            }

        }
        /**
         * App has been updated, do as normal run. Check for internet connection and download CSV file to populate DB.
         * If no internet connection, do not download or populate DB again as DB will already hold latest info.
         * Notify user of lack of internet connection.
         */
        else if (currentVersion > savedVersion) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            }else if(!isOnline()){
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();
            }


        }
        prefSettings.edit().putInt(PREF_VERSION_CODE, currentVersion).commit();
    }


}
