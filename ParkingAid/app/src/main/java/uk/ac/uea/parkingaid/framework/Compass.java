//package uk.ac.uea.parkingaid.framework;
//
//import android.content.Context;
//
///**
// * Created by ybm14yju on 02/12/2016.
// */
//
//public class Compass {
//
//    Context c;
//    public Compass(Context c){
//        this.c = c;
//    }
//
//    public float bearing(double startLat, double startLng, double endLat, double endLng){
//        double longitude1 = startLng;
//        double longitude2 = endLng;
//        double latitude1 = Math.toRadians(startLat);
//        double latitude2 = Math.toRadians(endLat);
//        double longDiff= Math.toRadians(longitude2-longitude1);
//        double y= Math.sin(longDiff)*Math.cos(latitude2);
//        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
//
//        return (float) (Math.toDegrees(Math.atan2(y, x))+360)%360;
//    }
//
//}
