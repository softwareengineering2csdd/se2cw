package uk.ac.uea.locationfinder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import uk.ac.uea.framework.implementation.AndroidFileIO;

/**
 * Created by ChanelleRichardson on 05/02/2017.
 */

public class CSVPopulateDBThread extends AsyncTask {
    Context context;
    AndroidFileIO afio;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;

    private static CSVPopulateDBThread instance = null;

    private CSVPopulateDBThread(Context context) {
        this.context = context;
    }

    public CSVPopulateDBThread(AndroidFileIO afio, Context context, SQLiteDatabase db) {
        this.context = context;
        this.afio = new AndroidFileIO(this.context);
        this.db = db;
    }

    public static CSVPopulateDBThread getInstance(Context context) {
        if (instance == null) {
            instance = new CSVPopulateDBThread(context);
        }
        return instance;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            insertFromCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * reads in the CSV file saved to context.getExternamFilesDir by the DownloadCSVFramework class.
     * starts a database transaction for each line,splits lines by commas, inserts them into the database then ends the transaction.
     * Faulty entries are caught in the catch block and the transaction is ended without setting it as successful.
     * Using transactions, no faulty entries can be entered into the database.
     * @throws IOException
     */
    public void insertFromCSV() throws IOException {

        dbHelper = DatabaseHelper.getInstance(context);
        db = dbHelper.getReadableDatabase();
        //BufferedReader buff = new BufferedReader(new InputStreamReader(afio.readFile("dataCSV.csv")));

        FileReader file = new FileReader(context.getExternalFilesDir("dataCSV.csv").toString());
        BufferedReader buff = new BufferedReader(file);

        String line;
        while ((line = buff.readLine()) != null) {
            try {
                db.beginTransaction();
                String[] entryData = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);


                int id = Integer.parseInt(entryData[0]);

                String buildingType = entryData[1];
                String name = entryData[2];
                String description = entryData[3];
                double latitude = Double.parseDouble(entryData[4]);
                double longitude = Double.parseDouble(entryData[5]);

                boolean favourite = false;

                dbHelper.insertOrReplaceLocationEntry(id, buildingType, name, description, latitude, longitude, favourite);

                db.setTransactionSuccessful();
                db.endTransaction();
            }
            catch (Exception e) {
                Log.e("CSV2DB entry failed:", e.toString());
                //Throw error to user if inserting an activity fails.
                db.endTransaction();

            }
        }
    }
}
