package uk.ac.uea.framework.implementation;

import android.app.Notification;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.ProgressDialog;
import android.widget.Toast;

import uk.ac.uea.locationfinder.MainActivity;

/**
 * Created by ChanelleRichardson on 03/02/2017.
 */

public class DownloadCSVFramework extends AsyncTask<String, Integer, String> {
    private Context context;
    private File file;
    //private ProgressDialog progDialog;
    //private ProgressDialog progDialog = new ProgressDialog(context);

    private static DownloadCSVFramework instance = null;


    private DownloadCSVFramework(Context context) {
        this.context = context;
        //progDialog = new ProgressDialog(context);
        //progDialog.show(this.context, "In progress", "Loading");

    }

    // singleton design pattern: only once instance can be created.
    public static DownloadCSVFramework getInstance(Context context) {
        if (instance == null) {
            instance = new DownloadCSVFramework(context);
        }
        return instance;
    }

    /**
     * get file URL passed in from the MainActivity call .execute("URL HERE").get();
     * check if connection is correct to avoid downloading a error log instead of the intended CSV file.
     * get file length to display a percentage on the progress bar
     * creates writable file within external file dir : /storage/emulated/0/Android/uk.ac.uea.locationfinder/files/ - Chanelle Nexus 6P
     * sets percentage to be shown for a progress dialog / progress bar
     * file output stream writes the data to the newly created dataCSV.csv.
     * @param fileURL
     * @return
     */
    @Override
    protected String doInBackground(String... fileURL) {
        if(android.os.Debug.isDebuggerConnected())
            android.os.Debug.waitForDebugger();
        InputStream is = null;
        FileOutputStream fos = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(fileURL[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

        //check if connection is correct to avoid downloading a error log instead of the intended CSV file.
            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage();
//                Log.d(TAG, "No network available!");

            }

            //get file length to display a percentage on the progress bar
            int fileLength = urlConnection.getContentLength();

            //download the CSV file using the input and output streams
            is = new BufferedInputStream(urlConnection.getInputStream());

            // creates writable file within external file dir : /storage/emulated/0/Android/uk.ac.uea.locationfinder/files/ - Chanelle Nexus 6P
            String dir = context.getExternalFilesDir(null).toString();
            String fileName = "dataCSV.csv";
            fos = new FileOutputStream(dir + "/" + fileName);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = is.read(data)) != -1) {
                if (isCancelled()) {
                    is.close();
                    return null;
                }
                total += count;
                publishProgress((int) (total * 100 / fileLength)); // percentage to be shown for a progress dialog / progress bar
                fos.write(data, 0, count); // Writes the data to the newly created dataCSV.csv.
            }
        } catch (Exception e) {
            Log.e("DLCSVFW: ", e.toString());
        } finally {
            try {
                if (fos != null) {
                    fos.flush();
                    fos.getFD().sync();
                    fos.close();
                }

                if (is != null)
                    is.close();
            } catch (IOException ignored) {
            }

            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        //progDialog.setProgress(progress[0]);
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progDialog.setMessage("Updating open day activities. Please wait.");
//        progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        progDialog.setCancelable(false);
//        progDialog.setCanceledOnTouchOutside(false);
//        progDialog.show();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
//        if (progDialog.isShowing()){
//            progDialog.dismiss();
//        }
//        progDialog.dismiss();

    }
}
