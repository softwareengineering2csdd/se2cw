package uk.ac.uea.parkingaid;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.view.menu.ListMenuPresenter;

/**
 * Created by Danny on 24/12/2016.
 */

public class AppSettings extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new AppSettingsFragment()).commit();
    }

    public static class AppSettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            //Set Default Value to "K" (First Option)
            ListPreference distancePref = (ListPreference)findPreference("distanceMetric");
            if(distancePref == null) {
                distancePref.setValueIndex(0);
            }

            //Set Default Value to 2mins (First Option)
            ListPreference notiPref = (ListPreference) findPreference("notificationTime");
            if(notiPref == null) {
                notiPref.setValueIndex(0);
            }
        }
    }
}
