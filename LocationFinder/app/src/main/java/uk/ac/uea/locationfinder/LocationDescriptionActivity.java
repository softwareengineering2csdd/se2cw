package uk.ac.uea.locationfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class LocationDescriptionActivity extends AppCompatActivity {
    private TextView txtLocationName;
    private TextView txtLocationDesc;
    private LocationModel lm;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_description);


        txtLocationName = (TextView)findViewById(R.id.txtLocationName);
        txtLocationDesc = (TextView)findViewById(R.id.txtLocationDesc);

        dbHelper = DatabaseHelper.getInstance(this);
        Intent i = getIntent();
        int iD = i.getIntExtra("locationID", 0);
        lm = dbHelper.getLocationEntryByID(iD);


        txtLocationName.setText(lm.getName());
        txtLocationDesc.setText(lm.getDescription());
    }

    public void viewMap(View view){
        Intent intent = new Intent(this, LocationMapsActivity.class);
        intent.putExtra("locationID", lm.getId());
        intent.putExtra("locationName", lm.getName());
        intent.putExtra("locationDescription", lm.getDescription());
        intent.putExtra("locationLat", lm.getLatitude());
        intent.putExtra("locationLong", lm.getLongitude());
        startActivity(intent);
    }
}
