package uk.ac.uea.activityprogram;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uk.ac.uea.framework.implementation.DownloadCSVFramework;
import uk.ac.uea.framework.implementation.PopulateDBFramework;

public class MainActivity extends AppCompatActivity {
    private DatabaseHelper activityDatabase;
    private static final String TAG = "mainActivity";
    private Spinner openDaySelector;
    private ListView listOfSchools;
    private String selectedDate;
    private String chosenDate = "";
    private Boolean isDBPopulated = false;

    /**
     * onCreate will make calls to methods to populate a spinner containing dates, and a list containing all available schools.
     * All data on dates and schools are retrieved from the database.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityDatabase = DatabaseHelper.getInstance(this);
        openDaySelector = (Spinner) findViewById(R.id.openDayDateSpn);
        listOfSchools = (ListView) findViewById(R.id.schoolsLst);

        /**
         * firstTimeRun() questions if this is the first time the app has run.
         * Will perform regular activities if its not the first time it has run.
         */
        firstTimeRun();

        loadSpinnerData(activityDatabase, openDaySelector);
        chosenDate = (String) openDaySelector.getItemAtPosition(0);
        loadListData(activityDatabase, chosenDate, listOfSchools);


        openDaySelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                chosenDate = (String) openDaySelector.getItemAtPosition(position);
                selectedDate = chosenDate;
                loadListData(activityDatabase, chosenDate, listOfSchools);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /**
         * Passes information over to the next activity through an intent.
         */
        listOfSchools.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedSchool = (String) (listOfSchools.getItemAtPosition(position));
                Log.d(TAG, "School =  " + selectedSchool);
                Intent i = new Intent(getApplicationContext(), SchoolActivity.class);
                i.putExtra("selectedSchool", selectedSchool);
                i.putExtra("selectedDate", chosenDate);
                startActivity(i);
            }
        });

    }


    public void loadSpinnerData(DatabaseHelper dbHelper, Spinner spn) {

        ArrayList<String> data = dbHelper.getAllOpenDays();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn.setAdapter(dataAdapter);
    }

    public void loadListData(DatabaseHelper dbHelper, String chosenDate, ListView lst) {

        ArrayList<String> data = dbHelper.getAllSchoolsOnOpenDay(chosenDate);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);

        lst.setAdapter(dataAdapter);

    }

    /**
     * async task version : populate database with downloaded CSV file from Google Spreadsheets.
     */

    public void populateDB() {
        try {
            CSVPopulateDBThread fileIO = CSVPopulateDBThread.getInstance(this);
            fileIO.execute().get();
        } catch (Exception e) {
            Log.e("PopulatingDB", e.toString());
        }
    }

    /**
     * backup version : populate database with pre defined CSV file from Google Spreadsheets if opening the app for 1st time & without internet access.
     */

    public void firstPopulationOfDB() {
        try {
            CSVFirstPopulateDB fileIO = CSVFirstPopulateDB.getInstance(this);
            fileIO.execute().get();
            Toast.makeText(this, "Connect to the internet to download the latest open day events", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e("PopulatingDB", e.toString());
        }
    }

    /**
     * Downloads the CSV file from Google Spreadsheets. Check if the internet is available before hand.
     * Downloading the CSV is a task given to a class implemented into the framework named DownloadCSVFramework.
     */

    public void downloadCSV() {
        if(isOnline()){
            try {
                DownloadCSVFramework DLCSV = DownloadCSVFramework.getInstance(this);
                DLCSV.execute("https://docs.google.com/spreadsheets/d/1RBwTQljo1FvdH0JskQdEcmIFonXDrgWkpyYMF6RiOjU/pub?output=csv").get();
            } catch (Exception e) {
                Log.e("DownloadCSV: ", e.toString());
            }
        }
    }

    /**
     * Checks that the device is connected to the internet.
     * @return true or false if there is or isnt an internet connection.
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(this, "Please connect to the internet to update", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * method checks the apps version code and saves it to the shared preferences.
     * based on App version (such as first run etc...), will check for an internet connection and populate the DB with correct data.
     */

    public void firstTimeRun() {
        final String PREFS_NAME = "firstTimeRunPref";
        final String PREF_VERSION_CODE = "versionCode";
        final int NO_EXISTING_VERSION = -1;
        SharedPreferences prefSettings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        int currentVersion = BuildConfig.VERSION_CODE;
        int savedVersion = prefSettings.getInt(PREF_VERSION_CODE, NO_EXISTING_VERSION);

        /**
         * App has been run before, version is the same. Check for internet connection and download CSV file to populate DB.
         * If no internet connection, do not download new CSV or populate DB again as DB will already hold local existing data.
         * Notify user of lack of internet connection. Can't download latest CSV.
         */

        if (currentVersion == savedVersion) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            }else if(!isOnline()){
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();
            }

        }
        /**
         * App has not been run before, check for internet connection and download new CSV file to populate DB.
         * If no internet connection, populate DB through CSV in assets folder.
         * Notify user of lack of internet connection if they wish to update DB entries
         */
        else if (savedVersion == NO_EXISTING_VERSION) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            } else if (!isOnline()) {
                firstPopulationOfDB();
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();

            }

        }
        /**
         * App has been updated, do as normal run. Check for internet connection and download CSV file to populate DB.
         * If no internet connection, do not download or populate DB again as DB will already hold latest info.
         * Notify user of lack of internet connection.
         */
        else if (currentVersion > savedVersion) {
            if (isOnline()) {
                downloadCSV();
                populateDB();
            }else if(!isOnline()){
                Toast.makeText(this, "No internet connection, please connect to the internet to update locations", Toast.LENGTH_LONG).show();
            }


        }
        prefSettings.edit().putInt(PREF_VERSION_CODE, currentVersion).commit();
    }

}
