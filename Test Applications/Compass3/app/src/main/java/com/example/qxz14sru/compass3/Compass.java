package com.example.qxz14sru.compass3;

import android.content.Context;
import android.hardware.SensorEventListener;

public class Compass {

    Context c;

    /**
     * Class Constructor
     * @param c
     */
    public Compass(Context c){
        this.c = c;
    }

    /**
     * Method to calculate bearing between to 2 sets of lat/long.
     * @param startLat
     * @param startLng
     * @param endLat
     * @param endLng
     * @return bearing
     */
    protected float bearing(double startLat, double startLng, double endLat, double endLng){
        double longitude1 = startLng;
        double longitude2 = endLng;
        double latitude1 = Math.toRadians(startLat);
        double latitude2 = Math.toRadians(endLat);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

        return (float) (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }
}
