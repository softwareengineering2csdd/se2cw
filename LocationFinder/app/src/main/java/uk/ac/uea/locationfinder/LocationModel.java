package uk.ac.uea.locationfinder;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChanelleRichardson on 18/01/2017.
 */

public class LocationModel {
    private int id;
    private String buildingType;
    private String name;
    private String description;
    private Double latitude;
    private Double longitude;
    private boolean favourite;

    public LocationModel() {
        this.name = null;
        this.buildingType = null;
    }

    /**
     * Regular constrictor that contains all the parameters needed to have a complete set of data about a location.
     * @param id
     * @param buildingType
     * @param name
     * @param description
     * @param latitude
     * @param longitude
     * @param favourite
     */
    public LocationModel(Integer id, String buildingType, String name, String description, double latitude, double longitude, boolean favourite) {
        this.id = id;
        this.name = name;
        this.buildingType = buildingType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.favourite = favourite;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }




}
