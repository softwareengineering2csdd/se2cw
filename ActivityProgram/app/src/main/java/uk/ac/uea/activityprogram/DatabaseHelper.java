
package uk.ac.uea.activityprogram;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.security.Key;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

import uk.ac.uea.framework.implementation.DateCreateAndConvert;

/**
 * Created by ChanelleRichardson on 21/11/2016.
 */


public class DatabaseHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 37;
    private static DatabaseHelper instance = null;
    private static Context context = null;
    DateCreateAndConvert conversion;

    private static final String TAG = "ActivityPlannerDb";

    protected DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static DatabaseHelper getInstance(Context context){
        if(instance == null){
            instance = new DatabaseHelper(context);
        }
        return instance;
    }


    private static final String DATABASE_NAME = "activityPlannerDB";

    //Tables
    private static final String TABLE_ACTIVITIES = "activities";
    private static final String TABLE_SCHOOLS = "schools";
    private static final String TABLE_SAVED_EVENTS = "savedEvents";

    //columns
    //repeated columns

    private static final String KEY_COLUMN_SCHOOLCODE = "schoolCode"; //sets school code as key for school table and school identifier in activities table.

    //Activities columns
    private static final String ACTIVITIES_COLUMN_ID = "id";
    private static final String ACTIVITIES_COLUMN_TITLE = "title";
    private static final String ACTIVITIES_COLUMN_ROOMNUMBER = "roomNumber";
    private static final String ACTIVITIES_COLUMN_TIME = "time";
    private static final String ACTIVITIES_COLUMN_OPENDAY = "openDay";
//    private static final String ACTIVITIES_COLUMN_SAVED = "saved";

    //Schools columns
    private static final String SCHOOLS_COLUMN_TITLE = "schoolTitle";
    private static final String SCHOOLS_COLUMN_FAVOURITE = "schoolFavourite";

    //saved events columns
    private static final String SAVED_EVENTS_COLUMN_ID = "id";
    private static final String SAVED_EVENTS_COLUMN_SAVED = "saved";

    //Create all table methods
//Activities Table creation
    private static final String CREATE_TABLE_ACTIVITIES = "CREATE TABLE " + TABLE_ACTIVITIES
            + "( " + ACTIVITIES_COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL, "
            + ACTIVITIES_COLUMN_TITLE + " TEXT, "
            + KEY_COLUMN_SCHOOLCODE + " TEXT,  "
            + ACTIVITIES_COLUMN_ROOMNUMBER + " TEXT, "
            + ACTIVITIES_COLUMN_TIME + " TEXT, "
            + ACTIVITIES_COLUMN_OPENDAY + " TEXT, "
//            + ACTIVITIES_COLUMN_SAVED + " BOOLEAN, "
//            + "FOREIGN KEY (" + KEY_COLUMN_SCHOOLCODE + ") REFERENCES " + TABLE_SCHOOLS + "(" + KEY_COLUMN_SCHOOLCODE + ")"
            + "FOREIGN KEY (" + ACTIVITIES_COLUMN_ID + ") REFERENCES " + TABLE_SAVED_EVENTS + "(" + SAVED_EVENTS_COLUMN_ID + ")"
            + ")";

//
    //Schools Table creation
    private static final String CREATE_TABLE_SCHOOLS = "CREATE TABLE " + TABLE_SCHOOLS
            + "( " + KEY_COLUMN_SCHOOLCODE + " TEXT PRIMARY KEY, "
//            + SCHOOLS_COLUMN_TITLE + " TEXT, "
            + SCHOOLS_COLUMN_FAVOURITE + " INTEGER " + ")";


    //Saved event Table creation
    private static final String CREATE_TABLE_SAVED_EVENTS = "CREATE TABLE " + TABLE_SAVED_EVENTS
            + "( " + SAVED_EVENTS_COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL, "
            + SAVED_EVENTS_COLUMN_SAVED + " INTEGER "
//            + "FOREIGN KEY (" + SAVED_EVENTS_COLUMN_ID + ") REFERENCES " + TABLE_ACTIVITIES + "(" + ACTIVITIES_COLUMN_ID + ")"
            + ")";

    //onCreate method
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ACTIVITIES);
        db.execSQL(CREATE_TABLE_SCHOOLS);
        db.execSQL(CREATE_TABLE_SAVED_EVENTS);


    }

    //onUpgrade method
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHOOLS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED_EVENTS);
        onCreate(db);
    }

    public int numberOfCol() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_ACTIVITIES, null);
        return res.getColumnCount();
    }

    public Date stringToDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy");
        ParsePosition pos = new ParsePosition(0);
        Date convertedDate = dateFormat.parse(date, pos);
        return convertedDate;
    }

    public String dateToString(Date date) {
        String sqlDatePattern = "EEE MMM dd kk:mm:ss z yyyy";
        String newDatePattern = "dd-MM-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(newDatePattern);
        String openDayString = dateFormat.format(date);
        return openDayString;
    }

    //TABLE_ACTIVITIES methods


    /**
     * INSERT OR REPLACE entry into the Activities table & SavedEvents table
     * db.replace: Will insert a new entry with the id specified or Update and entry that matches the id given to the method.
     * @param id
     * @param title
     * @param school
     * @param roomNumber
     * @param time
     * @param openDay
     * @param saved
     * @return
     */
    public boolean insertActivities(Integer id, String title, String school, String roomNumber, String time, String openDay, boolean saved) {
        SQLiteDatabase db = this.getWritableDatabase();
        Integer savedValue = saved? 1:0;

        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_ACTIVITIES + " WHERE ID=" + id + "", null);

        Cursor res2 = db.rawQuery("SELECT * FROM " + TABLE_SAVED_EVENTS + " WHERE ID=" + id + "", null);
        if (res==null && res2==null){

        }
        ContentValues contentValues = new ContentValues();
        ContentValues contentValues2 = new ContentValues();
        contentValues.put(ACTIVITIES_COLUMN_ID, id);
        contentValues.put(ACTIVITIES_COLUMN_TITLE, title);
        contentValues.put("schoolCode", school);
        contentValues.put("roomNumber", roomNumber);
        contentValues.put("time", time);
        contentValues.put("openDay",openDay);
        db.replace(TABLE_ACTIVITIES, null, contentValues);

        contentValues2.put("id", id);
        contentValues2.put("saved", savedValue);
        db.replace(TABLE_ACTIVITIES, null, contentValues2);

        return true;
    }

    /**
     * gets a single entry from the Activities table based on the id passed in.
     * returns the model of the single UEA Open Day Activity entry
     * @param id
     * @return
     */
    public ActivityModel getSingleActivity(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_ACTIVITIES + " WHERE ID=" + id + "", null);
        res.moveToFirst();


        ActivityModel am = new ActivityModel();
        am.setId(res.getInt(res.getColumnIndex(ACTIVITIES_COLUMN_ID)));
        am.setTitle(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TITLE)));
        am.setSchool(res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
        am.setRoomNumber(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_ROOMNUMBER)));
        am.setTime(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TIME)));
        am.setDate(stringToDate(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_OPENDAY))));
        boolean isSaved = getExistingSaved(id);
        am.setSaved(isSaved);
        return am;
    }

    /**
     * Gets all activities in the database table activities.
     * returns them in an array list
     * @return
     */
    public ArrayList<ActivityModel> getAllActivities() {
        ArrayList<ActivityModel> array_list = new ArrayList<ActivityModel>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_ACTIVITIES, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            int id = (res.getInt(res.getColumnIndex(ACTIVITIES_COLUMN_ID)));
            String title = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TITLE)));
            String school = (res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
            String roomNum = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_ROOMNUMBER)));
            String time = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TIME)));
            Date openDay = stringToDate(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_OPENDAY)));
            boolean isSaved = getExistingSaved(id);
            ActivityModel am = new ActivityModel(id, title, school, roomNum, time, openDay, isSaved);

            array_list.add(am);
            res.moveToNext();
        }
        return array_list;
    }

    /**
     * gets all distinct open day dates from the database.
     * returns dates in an array list
     * @return
     */
    public ArrayList<String> getAllOpenDays() {
        ArrayList<String> datesArrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT DISTINCT " + ACTIVITIES_COLUMN_OPENDAY + " FROM " + TABLE_ACTIVITIES, null);
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            datesArrayList.add(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_OPENDAY)));
            res.moveToNext();
        }
        return datesArrayList;
    }

    /**
     * gets a single UEA Open Day Activity from the database based on the id passed in
     * returns true if the UEA Open Day Activity is there.
     * @param givenID
     * @return
     */
    public boolean getExistingActivityByID(Integer givenID){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT DISTINCT " + ACTIVITIES_COLUMN_ID + " FROM "
                + TABLE_ACTIVITIES + " WHERE " + ACTIVITIES_COLUMN_ID + " = " +  givenID + "", null);
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            if (res.getInt(res.getColumnIndex(ACTIVITIES_COLUMN_ID)) == givenID){
                return true;
            }

            res.moveToNext();
        }
        return false;
    }


    /**
     * gets all distinct schools from the database where their UEA Open Day Activity also has the same date passed in.
     * returns dates in an array list
     * @param chosenOpenDay
     * @return
     */
    public ArrayList<String> getAllSchoolsOnOpenDay(String chosenOpenDay){
        ArrayList<String> schoolsArrayList = new ArrayList<String>();
//        ArrayList<String> favSchools = getAllFavouriteSchools();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("SELECT DISTINCT " + KEY_COLUMN_SCHOOLCODE + " FROM "
                + TABLE_ACTIVITIES + " WHERE " + ACTIVITIES_COLUMN_OPENDAY + " =?; ", new String[]{chosenOpenDay}, null);

//        Cursor res = db.rawQuery("SELECT DISTINCT " + KEY_COLUMN_SCHOOLCODE +
//                " FROM " + TABLE_ACTIVITIES +
//                " WHERE " + ACTIVITIES_COLUMN_OPENDAY + " =? " +
//                "AND "+ KEY_COLUMN_SCHOOLCODE + " !=? ", new String[]{chosenOpenDay, favSchools}, null);//       for (String school: favSchools) {
//            schoolsArrayList.add(school);
//        }

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            schoolsArrayList.add(res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
            res.moveToNext();
        }
        return schoolsArrayList;
    }

    /**
     * gets all UEA Open Day Activities from the database where the school and date are the same as those passed in.
     * @param school
     * @param selectedOpenDay
     * @return
     */
    public ArrayList<ActivityModel> getActivitiesBySchoolAndDate(String school, String selectedOpenDay){
        ArrayList<ActivityModel> activitiesArrayList = new ArrayList<ActivityModel>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("SELECT * FROM "
                + TABLE_ACTIVITIES +
//                " JOIN " + TABLE_SAVED_EVENTS + " ON " + ACTIVITIES_COLUMN_ID + " = " + SAVED_EVENTS_COLUMN_ID +
                " WHERE " + KEY_COLUMN_SCHOOLCODE + " =? AND " + ACTIVITIES_COLUMN_OPENDAY + " =?;", new String[]{school, selectedOpenDay}, null);

        res.moveToFirst();
        try {
            while (res.isAfterLast() == false) {
                int id = (res.getInt(res.getColumnIndex(ACTIVITIES_COLUMN_ID)));
                String title = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TITLE)));
                String schoolcode = (res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
                String roomNum = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_ROOMNUMBER)));
                String time = (res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_TIME)));
                Date openDay = stringToDate(res.getString(res.getColumnIndex(ACTIVITIES_COLUMN_OPENDAY)));


                boolean isSaved = getExistingSaved(id);
//                boolean isSaved = res.getInt(res.getColumnIndex(SAVED_EVENTS_COLUMN_SAVED)) !=0;

//                Log.d("OPENDAY: "+ selectedOpenDay , ""+isSaved);

                ActivityModel am = new ActivityModel(id, title, schoolcode, roomNum, time, openDay, isSaved);
                activitiesArrayList.add(am);
                res.moveToNext();
                Log.d("UEA ODA:", "processed DBEntry");
            }
        }
        catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }
        res.close();
        return activitiesArrayList;
    }

    /**
     * updates an UEA Open Day Activity of the corresponding id passed in.
     * @param id
     * @param title
     * @param school
     * @param roomNumber
     * @param time
     * @param openDay
     * @param saved
     * @return
     */
    //Takes a Date type for the open day date.
    public boolean updateActivities(Integer id, String title, String school, String roomNumber, String time, String openDay, boolean saved) {
        SQLiteDatabase db = this.getWritableDatabase();
        Integer savedValue = saved? 1:0;

        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("title", title);
        contentValues.put("schoolCode", school);
        contentValues.put("roomNumber", roomNumber);
        contentValues.put("time", time);
        //contentValues.put("openDay", dateToString(openDay));
        contentValues.put("openDay", openDay);

        ContentValues savedContentValues = new ContentValues();
        savedContentValues.put("id", id);
        savedContentValues.put("saved", savedValue); //TODO set to something else so that users saved open day events will not be reset to false after every database update.
        Log.d("DB is saved?: ", ""+saved);
        db.update(TABLE_ACTIVITIES, contentValues, "ID = ? ", new String[]{Integer.toString(id)});

        return true;
    }

    public Integer deleteActivity(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_ACTIVITIES,
                "ID = ? ",
                new String[]{Integer.toString(id)});
    }


    //TABLE_SCHOOLS methods
//    public boolean insertFavouriteSchool(String schoolCode, String schoolTitle, boolean schoolFavourite) {

    /**
     * inserts or replaces a favourite school into the schools table.
     * @param schoolCode
     * @param schoolFavourite
     * @return
     */
    public boolean insertFavouriteSchool(String schoolCode, boolean schoolFavourite) {
        SQLiteDatabase db = this.getWritableDatabase();
        Integer savedValue = schoolFavourite? 1:0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_COLUMN_SCHOOLCODE, schoolCode);
//        contentValues.put(SCHOOLS_COLUMN_TITLE, schoolTitle);
        contentValues.put(SCHOOLS_COLUMN_FAVOURITE, savedValue);
        db.replace(TABLE_SCHOOLS, null, contentValues);
        return true;
    }

    /**
     * returns an array list of all of the favourite schools in the schools table
     * @return
     */
        public ArrayList<String> getAllFavouriteSchools() {
        ArrayList<String> favSchool = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_SCHOOLS, null);
        res.moveToFirst();
            while(res.isAfterLast()==false){
                favSchool.add(res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
            }

        return favSchool;
    }
//
//
//    public SchoolModel getSingleSchool(String schoolCode) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_SCHOOLS + "WHERE " + KEY_COLUMN_SCHOOLCODE + " =" + schoolCode + "", null);
//        SchoolModel sm = new SchoolModel();
//        sm.setSchoolCode(res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
//        sm.setSchoolTitle(res.getString(res.getColumnIndex(SCHOOLS_COLUMN_TITLE)));
//        sm.setSchoolDescription(res.getString(res.getColumnIndex(SCHOOLS_COLUMN_DESCRIPTION)));
//        return sm;
//    }
//
////    public Cursor getSingleSchoolCode(String schoolCode) {
////        SQLiteDatabase db = this.getReadableDatabase();
////        Cursor res = db.rawQuery("SELECT " + KEY_COLUMN_SCHOOLCODE + " FROM " + TABLE_SCHOOLS + "WHERE " + KEY_COLUMN_SCHOOLCODE + " =" + schoolCode + "", null);
////        return res;
////    }
////
//    public ArrayList<SchoolModel> getAllFavouriteSchools() {
//        ArrayList<SchoolModel> array_list = new ArrayList<SchoolModel>();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res = db.rawQuery("select * from " + TABLE_SCHOOLS, null);
//        res.moveToFirst();
////        SchoolModel sm = new SchoolModel();
//        sm.setSchoolCode(res.getString(res.getColumnIndex(KEY_COLUMN_SCHOOLCODE)));
////        sm.setSchoolTitle(res.getString(res.getColumnIndex(SCHOOLS_COLUMN_TITLE)));
//        sm.setSchoolDescription(res.getString(res.getColumnIndex(SCHOOLS_COLUMN_FAVOURITE)));
//
//        array_list.add(sm);
//        return array_list;
//    }
//
//
//    public boolean updateSchools(String schoolCode, String schoolTitle, String schoolDescription) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("schoolCode", schoolCode);
//        contentValues.put("schoolTitle", schoolTitle);
//        contentValues.put("schoolDescription", schoolDescription);
//
//        db.update(TABLE_SCHOOLS, contentValues, KEY_COLUMN_SCHOOLCODE + " = ? ", new String[]{schoolCode});
//        return true;
//    }
//
//    public Integer deleteAFavouriteSchool(String schoolCode) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        return db.delete(TABLE_SCHOOLS,
//                KEY_COLUMN_SCHOOLCODE + " = ? ",
//                new String[]{schoolCode});
//    }


    //TABLE SAVED EVENTS methods

    /**
     * insert or replace a saved event. return true when complete
     * @param id
     * @param saved
     * @return
     */
    public boolean insertSavedEvent(Integer id, Boolean saved) {
        SQLiteDatabase db = this.getWritableDatabase();
        Integer savedValue = saved? 1:0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(SAVED_EVENTS_COLUMN_ID, id);
        contentValues.put(SAVED_EVENTS_COLUMN_SAVED, savedValue);
        db.replace(TABLE_SAVED_EVENTS, null, contentValues);
        return true;
    }

    /**
     * gets a single saved UEA OpenDay Activity that matches the passed in id from the savedEvent table
     * @param ID
     * @return
     */
    public Cursor getSingleOpenDayRow(Integer ID) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_SAVED_EVENTS + "WHERE ID=" + ID + "", null);
        return res;
    }

    /**
     * gets all saved UEA OpenDay Activities from the savedEvent table
     * @return
     */
    public ArrayList<String> getAllSavedEvents() {
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_SAVED_EVENTS, null);
        res.moveToFirst();
        return array_list;
    }

    /**
     * Update a saved UEA OpenDay Activity that matches the passed in id from the savedEvent table.
     * @param id
     * @param saved
     * @return
     */
    public boolean updateSavedEvent(Integer id, Boolean saved) {
        SQLiteDatabase db = this.getWritableDatabase();
        Integer savedValue = saved? 1:0;

        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put(SAVED_EVENTS_COLUMN_SAVED, savedValue);

        db.update(TABLE_SAVED_EVENTS, contentValues, "ID = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    /**
     * delete a saved event from the database
     * @param ID
     * @return
     */
    public Integer deleteSavedEvent(Integer ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_SAVED_EVENTS,
                "ID = ? ",
                new String[]{Integer.toString(ID)});
    }

    /**
     * check if a saved UEA OpenDay Activity that matches the passed in id is in the savedEvent table
     * @param givenID
     * @return
     */
    public Boolean getExistingSaved(Integer givenID) {
        boolean isSaved = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_SAVED_EVENTS + " WHERE id = " + givenID + "", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            isSaved = res.getInt(res.getColumnIndex(SAVED_EVENTS_COLUMN_SAVED)) !=0;
            res.moveToNext();
        }
        return isSaved;
    }

    /**
     * return the size of the SavedEvent table.
     * @return
     */
    public Integer getSavedTSize(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + TABLE_SAVED_EVENTS + "", null);
        int size = cursor.getCount();
        cursor.close();
        return size;
    }
}
