package uk.ac.uea.framework.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;

import uk.ac.uea.framework.FileIO;
import uk.ac.uea.framework.Input;

/**
 * Used to read and write files
 */
public class AndroidFileIO implements FileIO {
    Context context;
    AssetManager assets;
    String externalStoragePath;


    public AndroidFileIO(Context context) {
        this.context = context;
        this.assets = context.getAssets();
        this.externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
        //this.externalStoragePath = "/Android/data/" + context.getPackageName() + "/files/";
        //this.externalStoragePath = context.getExternalFilesDir().toString();
    }

    @Override
    public InputStream readAsset(String file) throws IOException {
        return assets.open(file);
    }

    @Override
    public InputStream readFile(String file) throws IOException {
        return new FileInputStream(externalStoragePath + file);
    }

    @Override
    public OutputStream writeFile(String file) throws IOException {
        return new FileOutputStream(externalStoragePath + file);
    }
    
    public SharedPreferences getSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public BufferedReader FRReadAsset(String file) throws IOException {
//        File starterDataFile = new File(assets.open(file));
//        return new FileReader(starterDataFile);
        BufferedReader br =new BufferedReader(new InputStreamReader(assets.open(file)));
        return br;
    }


}
