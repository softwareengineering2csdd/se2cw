package uk.ac.uea.locationfinder;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import uk.ac.uea.framework.implementation.DeviceLocation;

public class LocationMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double latitude;
    private double longitude;
    private LatLngBounds UEA = new LatLngBounds(
            new LatLng(52.591806, 1.203349), new LatLng(52.660460, 1.280164));
    private String description;
    private DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
    private DeviceLocation location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        location = new DeviceLocation(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        Intent i = getIntent();
        int locationID = i.getIntExtra("locationID", 0);
        dbHelper = DatabaseHelper.getInstance(this);
        LocationModel lm = dbHelper.getLocationEntryByID(locationID);

        latitude = lm.getLatitude();
        longitude = lm.getLongitude();
        description = lm.getName(); // lm.getName is used to display location title in maps description box. Maps calls this text box 'description'
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(15.0f);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UEA.getCenter(), 15));
        mMap.setLatLngBoundsForCameraTarget(UEA);

        // Add a marker in Sydney and move the camera
        LatLng point = new LatLng(latitude, longitude);
        LatLng deviceLoc = new LatLng(location.getLatitude(), location.getLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder().target(point).zoom(16).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.addMarker(new MarkerOptions().position(point).title(description));
        mMap.addMarker(new MarkerOptions().position(deviceLoc).title("You are Here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))).showInfoWindow();
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    }


}
